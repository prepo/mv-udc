<h1 align="center">MV-UDC</h1> <br>
<!--<p align="center">
    <img alt="mv-udc logo" title="mv-udc logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>Universal Decimal Classification searching</strong>
</div>
<div align="center">
  Browse Universal Decimal Classification in the Valais Media Library for the library staff.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-udc/">Access</a>
    <span> | </span>
    <a href="#">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-udc/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [Release History](#release-history)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-udc/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-udc/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-udc/commits/master)


👉 [View online](https://valais-media-library.gitlab.io/mv-udc/)

## Features

- Browse ans search UDC labels
- link to the catalog

## Documentation

Comming…

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-udc/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-udc/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG.md](https://gitlab.com/valais-media-library/mv-udc/blob/master/CHANGELOG.md) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://github.com/michaelravedoni/prathletics/contributors) who participated in this project.

* The [**UDC Summary**](http://www.udcsummary.info/about.htm) (UDCS) [[view](http://www.udcsummary.info/php/index.php?id=13358&lang=en)] [[licences](http://www.udcc.org/index.php/site/page?view=licences)] providing a selection of around 2,600 classes from the whole scheme which comprises more than 70,000 entries. Data provided in this Summary is released under the [Creative Commons Attribution Share Alike 3.0 license](https://creativecommons.org/licenses/by-sa/3.0/).
* [Vue.js](https://github.com/vuejs/vue) Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.

## License

[MIT License](https://gitlab.com/valais-media-library/mv-udc/blob/master/LICENSE)
